#! /bin/sh
### BEGIN INIT INFO
# Provides: shishi-kdc
# Required-Start: $remote_fs $network $syslog $time
# Required-Stop: $remote_fs $network $syslog $time
# Default-Start: 2 3 4 5
# Default-Stop: 0 1 6
# Short-Description: shishid - Kerberos Key Distribution Center (KDC)
# Description: 
#	Start/stop the kerberos authentication service
#	main server, the Key Distribution Center (KDC)
### END INIT INFO

# Author: Simon Josefsson <simon@josefsson.org>

# Some comments about above choosing:
# At start:
#    $remote_fs: So /usr and all other filesystems are there.
#    $network: Probably better, when the interfaces are up.
#    $syslog: So we can feed our important logs somewhere at startup.
#             You want to know, why it's not starting, right?
# The above reasons mainly count for stop as well, so here some
# extra notes:
#    $remote_fs: So we can close files cleanly.

# Do NOT "set -e"

# PATH should only include /usr/* if it runs after the mountnfs.sh script
PATH=/usr/sbin:/usr/bin:/sbin:/bin
DESC="Shishi Kerberos v5 KDC daemon"
NAME=shishid
DAEMON=/usr/sbin/$NAME
DAEMON_ARGS=""
PIDFILE=/var/run/$NAME.pid
SCRIPTNAME=/etc/init.d/shishi-kdc

# Read configuration variable file if it is present
[ -r /etc/default/shishi-kdc ] && . /etc/default/shishi-kdc

# Exit if the package is not installed
[ -x "$DAEMON" ] || exit 0

# Load the VERBOSE setting and other rcS variables
[ -f /etc/default/rcS ] && . /etc/default/rcS

# Define LSB log_* functions.
. /lib/lsb/init-functions

#
# Function that starts the daemon/service
#
do_start()
{
	start-stop-daemon --start --oknodo --background --quiet \
	    --pidfile $PIDFILE --make-pidfile --exec $DAEMON -- \
	    $DAEMON_ARGS $DAEMON_OPTS
}

#
# Function that stops the daemon/service
#
do_stop()
{
	start-stop-daemon --stop --oknodo --quiet \
	    --pidfile $PIDFILE --retry=TERM/30/KILL/5 --exec $DAEMON
	RETVAL="$?"
	# Many daemons don't delete their pidfiles when they exit.
	rm -f $PIDFILE
	return "$RETVAL"
}

case "$1" in
  status)
	status_of_proc "$DAEMON" "$NAME" && exit 0 || exit $?
	;;
  start)
	[ "$VERBOSE" != no ] && log_daemon_msg "Starting $DESC" "$NAME"
	do_start
	case "$?" in
		0|1) [ "$VERBOSE" != no ] && log_end_msg 0 ;;
		2) [ "$VERBOSE" != no ] && log_end_msg 1 ;;
	esac
	;;
  stop)
	[ "$VERBOSE" != no ] && log_daemon_msg "Stopping $DESC" "$NAME"
	do_stop
	case "$?" in
		0|1) [ "$VERBOSE" != no ] && log_end_msg 0 ;;
		2) [ "$VERBOSE" != no ] && log_end_msg 1 ;;
	esac
	;;
  restart|force-reload)
	log_daemon_msg "Restarting $DESC" "$NAME"
	do_stop
	case "$?" in
	  0|1)
		do_start
		case "$?" in
			0) log_end_msg 0 ;;
			1) log_end_msg 1 ;; # Old process is still running
			*) log_end_msg 1 ;; # Failed to start
		esac
		;;
	  *)
	  	# Failed to stop
		log_end_msg 1
		;;
	esac
	;;
  *)
	echo "Usage: $SCRIPTNAME {start|stop|restart|force-reload}" >&2
	exit 3
	;;
esac

:
