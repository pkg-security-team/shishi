<?xml version="1.0"?>
<!DOCTYPE book PUBLIC "-//OASIS//DTD DocBook XML V4.3//EN"
               "http://www.oasis-open.org/docbook/xml/4.3/docbookx.dtd"
[
  <!ENTITY % local.common.attrib "xmlns:xi  CDATA  #FIXED 'http://www.w3.org/2003/XInclude'">
  <!ENTITY % gtkdocentities SYSTEM "xml/gtkdocentities.ent">
  %gtkdocentities;
]>
<book id="index">
  <bookinfo>
    <title>GNU Shishi API Reference Manual</title>
    <releaseinfo>
      for &package_string;.
      The latest version of this documentation can be found on-line at
      <ulink role="online-location" url="&package_url;reference/">
	&package_url;reference/</ulink>.
    </releaseinfo>
  </bookinfo>

  <chapter id="intro">
    <title>GNU Shishi API Reference Manual</title>

    <para>
Shishi is an implementation of the Kerberos 5 network authentication
system, as specified in RFC 4120.  Shishi can be used to authenticate
users in distributed systems.
    </para>

    <para>
Shishi contains a library ('libshishi') that can be used by
application developers to add support for Kerberos 5.  Shishi contains
a command line utility ('shishi') that is used by users to acquire and
manage tickets (and more).  The server side, a Key Distribution
Center, is implemented by 'shishid'.  Of course, a manual documenting
usage aspects as well as the programming API is included.
    </para>

    <para>
Shishi currently supports AS/TGS exchanges for acquiring tickets,
pre-authentication, the AP exchange for performing client and server
authentication, and SAFE/PRIV for integrity/privacy protected
application data exchanges.
    </para>

    <para>
Shishi is internationalized; error and status messages can be
translated into the users' language; user name and passwords can be
converted into any available character set (normally including
ISO-8859-1 and UTF-8) and also be processed using an experimental
Stringprep profile.
    </para>

    <para>
Most, if not all, of the widely used encryption and checksum types are
supported, such as 3DES, AES, ARCFOUR and HMAC-SHA1.
    </para>

    <para>
Shishi is developed for the GNU/Linux system, but runs on over 20
platforms including most major Unix platforms and Windows, and many
kind of devices including iPAQ handhelds and S/390 mainframes.
    </para>

    <para>
Shishi is free software licensed under the GNU General Public License
version 3.0 (or later).
    </para>

    <figure id="components">
      <title>Source code layout of Shishi</title>
      <graphic fileref="components.png" format="PNG"></graphic>
    </figure>

    <xi:include href="xml/shishi.xml"/>
    <xi:include href="xml/shishi-version.xml"/>

  </chapter>
  <index id="api-index-full">
    <title>API Index</title>
    <xi:include href="xml/api-index-full.xml"><xi:fallback /></xi:include>
  </index>
  <index id="api-index-0-0-42" role="0.0.42">
    <title>Index of new API in 0.0.42</title>
    <xi:include href="xml/api-index-0.0.42.xml"><xi:fallback /></xi:include>
  </index>
  <index id="api-index-1-0-3" role="1.0.3">
    <title>Index of new API in 1.0.3</title>
    <xi:include href="xml/api-index-1.0.3.xml"><xi:fallback /></xi:include>
  </index>
  <index id="deprecated-api-index" role="deprecated">
    <title>Index of deprecated API</title>
    <xi:include href="xml/api-index-deprecated.xml"><xi:fallback /></xi:include>
  </index>
</book>
