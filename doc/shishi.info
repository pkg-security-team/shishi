This is shishi.info, produced by makeinfo version 6.7 from shishi.texi.

This manual is last updated 7 August 2022 for version 1.0.3 of Shishi.

   Copyright (C) 2002-2022 Simon Josefsson.

     Permission is granted to copy, distribute and/or modify this
     document under the terms of the GNU Free Documentation License,
     Version 1.3 or any later version published by the Free Software
     Foundation; with no Invariant Sections, no Front-Cover Texts, and
     no Back-Cover Texts.  A copy of the license is included in the
     section entitled "GNU Free Documentation License".
INFO-DIR-SECTION GNU utilities
START-INFO-DIR-ENTRY
* shishi: (shishi).		A Kerberos 5 implementation
END-INFO-DIR-ENTRY

INFO-DIR-SECTION GNU Libraries
START-INFO-DIR-ENTRY
* libshishi: (shishi).		Library implementing Kerberos 5.
END-INFO-DIR-ENTRY


Indirect:
shishi.info-1: 839
shishi.info-2: 302108
shishi.info-3: 606356

Tag Table:
(Indirect)
Node: Top839
Node: Introduction2207
Node: Getting Started3965
Node: Features and Status4895
Node: Overview9791
Ref: Overview-Footnote-115067
Node: Cryptographic Overview15265
Ref: Cryptographic Overview-Footnote-125668
Ref: Cryptographic Overview-Footnote-225727
Node: Supported Platforms25785
Node: Getting help28733
Node: Downloading and Installing29204
Node: Bug Reports31072
Node: Contributing32465
Node: User Manual34592
Node: Administration Manual47378
Ref: Administration Manual-Footnote-148908
Node: Introduction to Shisa48987
Node: Configuring Shisa51508
Node: Using Shisa52942
Node: Starting Shishid63253
Node: Configuring DNS for KDC68180
Node: Kerberos via TLS72746
Node: Multiple servers87815
Node: Developer information93014
Node: Reference Manual94321
Node: Environmental Assumptions95472
Node: Glossary of terms97694
Node: Realm and Principal Naming103020
Ref: krbtgt111452
Node: Shishi Configuration116955
Ref: realm-kdc119310
Node: Shisa Configuration122385
Node: Parameters for shishi125105
Node: Parameters for shishid131035
Node: Parameters for shisa133410
Node: Environment variables137009
Node: Date input formats138150
Node: General date syntax140542
Node: Calendar date items143528
Node: Time of day items145442
Node: Time zone items147647
Node: Combined date and time of day items148999
Node: Day of week items149863
Node: Relative items in date strings150880
Node: Pure numbers in date strings153691
Node: Seconds since the Epoch154681
Node: Specifying time zone rules156376
Node: Authors of parse_datetime159690
Ref: Authors of get_date159878
Node: Programming Manual160841
Node: Preparation162454
Node: Header163145
Node: Initialization163763
Node: Version Check164444
Ref: shishi_check_version164978
Node: Building the source165869
Node: Autoconf tests167781
Node: Initialization Functions170763
Ref: shishi170965
Ref: shishi_server171347
Ref: shishi_done171733
Ref: shishi_init172368
Ref: shishi_init_with_paths173243
Ref: shishi_init_server174509
Ref: shishi_init_server_with_paths175222
Ref: shishi_cfg176039
Ref: shishi_cfg_from_file176467
Ref: shishi_cfg_print176845
Ref: shishi_cfg_default_systemfile177259
Ref: shishi_cfg_default_userdirectory177857
Ref: shishi_cfg_userdirectory_file178619
Ref: shishi_cfg_default_userfile179480
Ref: shishi_cfg_clientkdcetype180013
Ref: shishi_cfg_clientkdcetype_fast180546
Ref: shishi_cfg_clientkdcetype_set181052
Ref: shishi_cfg_authorizationtype_set181735
Node: Ticket Set Functions182429
Ref: shishi_tkts_default_file_guess183343
Ref: shishi_tkts_default_file183789
Ref: shishi_tkts_default_file_set184192
Ref: shishi_tkts_default184688
Ref: shishi_tkts184969
Ref: shishi_tkts_done185300
Ref: shishi_tkts_size185637
Ref: shishi_tkts_nth185904
Ref: shishi_tkts_remove186440
Ref: shishi_tkts_add186886
Ref: shishi_tkts_new187323
Ref: shishi_tkts_read188001
Ref: shishi_tkts_from_file188365
Ref: shishi_tkts_write188744
Ref: shishi_tkts_expire189080
Ref: shishi_tkts_to_file189361
Ref: shishi_tkts_print_for_service189731
Ref: shishi_tkts_print190242
Ref: shishi_tkt_match_p190582
Ref: shishi_tkts_find190946
Ref: shishi_tkts_find_for_clientserver192308
Ref: shishi_tkts_find_for_server192863
Ref: shishi_tkts_get_tgt193395
Ref: shishi_tkts_get_tgs194272
Ref: shishi_tkts_get194931
Ref: shishi_tkts_get_for_clientserver195701
Ref: shishi_tkts_get_for_server196224
Ref: shishi_tkts_get_for_localservicepasswd196730
Node: AP-REQ and AP-REP Functions197322
Ref: shishi_ap198497
Ref: shishi_ap_etype199213
Ref: shishi_ap_nosubkey199703
Ref: shishi_ap_done200087
Ref: shishi_ap_set_tktoptions200430
Ref: shishi_ap_set_tktoptionsdata200915
Ref: shishi_ap_set_tktoptionsraw201613
Ref: shishi_ap_set_tktoptionsasn1usage202453
Ref: shishi_ap_tktoptions203379
Ref: shishi_ap_tktoptionsdata204093
Ref: shishi_ap_tktoptionsraw205027
Ref: shishi_ap_etype_tktoptionsdata206100
Ref: shishi_ap_tktoptionsasn1usage207129
Ref: shishi_ap_tkt208278
Ref: shishi_ap_tkt_set208596
Ref: shishi_ap_authenticator_cksumdata208887
Ref: shishi_ap_authenticator_cksumdata_set209606
Ref: shishi_ap_authenticator_cksumraw_set210422
Ref: shishi_ap_authenticator_cksumtype211327
Ref: shishi_ap_authenticator_cksumtype_set211682
Ref: shishi_ap_authenticator212035
Ref: shishi_ap_authenticator_set212412
Ref: shishi_ap_req212716
Ref: shishi_ap_req_set213049
Ref: shishi_ap_req_der213310
Ref: shishi_ap_req_der_set213895
Ref: shishi_ap_req_build214366
Ref: shishi_ap_req_decode214682
Ref: shishi_ap_req_process_keyusage215014
Ref: shishi_ap_req_process215805
Ref: shishi_ap_req_asn1216330
Ref: shishi_ap_key216681
Ref: shishi_ap_rep217043
Ref: shishi_ap_rep_set217376
Ref: shishi_ap_rep_der217637
Ref: shishi_ap_rep_der_set218211
Ref: shishi_ap_rep_build218682
Ref: shishi_ap_rep_asn1218994
Ref: shishi_ap_rep_verify219359
Ref: shishi_ap_rep_verify_der219675
Ref: shishi_ap_rep_verify_asn1220232
Ref: shishi_ap_encapreppart220676
Ref: shishi_ap_encapreppart_set221048
Ref: shishi_ap_option2string221367
Ref: shishi_ap_string2option221916
Ref: shishi_apreq222307
Ref: shishi_apreq_print222623
Ref: shishi_apreq_save223013
Ref: shishi_apreq_to_file223392
Ref: shishi_apreq_parse223958
Ref: shishi_apreq_read224408
Ref: shishi_apreq_from_file224853
Ref: shishi_apreq_set_authenticator225429
Ref: shishi_apreq_add_authenticator226407
Ref: shishi_apreq_set_ticket227075
Ref: shishi_apreq_options227493
Ref: shishi_apreq_use_session_key_p227958
Ref: shishi_apreq_mutual_required_p228381
Ref: shishi_apreq_options_set228792
Ref: shishi_apreq_options_add229232
Ref: shishi_apreq_options_remove229732
Ref: shishi_apreq_get_authenticator_etype230263
Ref: shishi_apreq_get_ticket230702
Ref: shishi_aprep231109
Ref: shishi_aprep_print231432
Ref: shishi_aprep_save231822
Ref: shishi_aprep_to_file232201
Ref: shishi_aprep_parse232767
Ref: shishi_aprep_read233217
Ref: shishi_aprep_from_file233662
Ref: shishi_aprep_get_enc_part_etype234240
Ref: shishi_encapreppart234661
Ref: shishi_encapreppart_print235099
Ref: shishi_encapreppart_save235541
Ref: shishi_encapreppart_to_file235967
Ref: shishi_encapreppart_parse236580
Ref: shishi_encapreppart_read237077
Ref: shishi_encapreppart_from_file237569
Ref: shishi_encapreppart_get_key238182
Ref: shishi_encapreppart_ctime238624
Ref: shishi_encapreppart_ctime_set239124
Ref: shishi_encapreppart_cusec_get239619
Ref: shishi_encapreppart_cusec_set240124
Ref: shishi_encapreppart_seqnumber_get240632
Ref: shishi_encapreppart_seqnumber_remove241155
Ref: shishi_encapreppart_seqnumber_set241592
Ref: shishi_encapreppart_time_copy242120
Node: SAFE and PRIV Functions242581
Ref: shishi_safe243667
Ref: shishi_safe_done244021
Ref: shishi_safe_key244360
Ref: shishi_safe_key_set244704
Ref: shishi_safe_safe244979
Ref: shishi_safe_safe_set245334
Ref: shishi_safe_safe_der245646
Ref: shishi_safe_safe_der_set246263
Ref: shishi_safe_print246738
Ref: shishi_safe_save247119
Ref: shishi_safe_to_file247489
Ref: shishi_safe_parse248046
Ref: shishi_safe_read248487
Ref: shishi_safe_from_file248923
Ref: shishi_safe_cksum249466
Ref: shishi_safe_set_cksum250142
Ref: shishi_safe_user_data251019
Ref: shishi_safe_set_user_data251665
Ref: shishi_safe_build252179
Ref: shishi_safe_verify252669
Ref: shishi_priv254212
Ref: shishi_priv_done254566
Ref: shishi_priv_key254905
Ref: shishi_priv_key_set255238
Ref: shishi_priv_priv255513
Ref: shishi_priv_priv_set255865
Ref: shishi_priv_priv_der256177
Ref: shishi_priv_priv_der_set256794
Ref: shishi_priv_encprivpart257281
Ref: shishi_priv_encprivpart_set257670
Ref: shishi_priv_encprivpart_der258023
Ref: shishi_priv_encprivpart_der_set258603
Ref: shishi_priv_print259097
Ref: shishi_priv_save259478
Ref: shishi_priv_to_file259848
Ref: shishi_priv_parse260405
Ref: shishi_priv_read260846
Ref: shishi_priv_from_file261282
Ref: shishi_priv_enc_part_etype261843
Ref: shishi_priv_set_enc_part262263
Ref: shishi_encprivpart_user_data263163
Ref: shishi_encprivpart_set_user_data263861
Ref: shishi_priv_build264413
Ref: shishi_priv_process264901
Node: Ticket Functions265373
Ref: shishi_tkt265952
Ref: shishi_tkt2266264
Ref: shishi_tkt_done266792
Ref: shishi_tkt_ticket267050
Ref: shishi_tkt_ticket_set267308
Ref: shishi_tkt_enckdcreppart267596
Ref: shishi_tkt_enckdcreppart_set267897
Ref: shishi_tkt_kdcrep268216
Ref: shishi_tkt_encticketpart268486
Ref: shishi_tkt_encticketpart_set268784
Ref: shishi_tkt_key269075
Ref: shishi_tkt_key_set269478
Ref: shishi_tkt_client269788
Ref: shishi_tkt_client_p270604
Ref: shishi_tkt_clientrealm270957
Ref: shishi_tkt_clientrealm_p271800
Ref: shishi_tkt_realm272203
Ref: shishi_tkt_server272645
Ref: shishi_tkt_server_p273453
Ref: shishi_tkt_flags273794
Ref: shishi_tkt_flags_set274127
Ref: shishi_tkt_flags_add274546
Ref: shishi_tkt_forwardable_p274930
Ref: shishi_tkt_forwarded_p275742
Ref: shishi_tkt_proxiable_p276465
Ref: shishi_tkt_proxy_p277301
Ref: shishi_tkt_may_postdate_p277854
Ref: shishi_tkt_postdated_p279150
Ref: shishi_tkt_invalid_p279959
Ref: shishi_tkt_renewable_p280826
Ref: shishi_tkt_initial_p281391
Ref: shishi_tkt_pre_authent_p282133
Ref: shishi_tkt_hw_authent_p282855
Ref: shishi_tkt_transited_policy_checked_p283621
Ref: shishi_tkt_ok_as_delegate_p285500
Ref: shishi_tkt_keytype286549
Ref: shishi_tkt_keytype_fast286932
Ref: shishi_tkt_keytype_p287291
Ref: shishi_tkt_lastreqc287715
Ref: shishi_tkt_authctime288194
Ref: shishi_tkt_startctime288615
Ref: shishi_tkt_endctime289014
Ref: shishi_tkt_renew_tillc289385
Ref: shishi_tkt_valid_at_time_p289791
Ref: shishi_tkt_valid_now_p290195
Ref: shishi_tkt_expired_p290484
Ref: shishi_tkt_lastreq_pretty_print290797
Ref: shishi_tkt_pretty_print291152
Node: AS Functions291400
Ref: shishi_as293541
Ref: shishi_as_done293872
Ref: shishi_as_req294193
Ref: shishi_as_req_build294547
Ref: shishi_as_req_set294821
Ref: shishi_as_req_der295082
Ref: shishi_as_req_der_set295607
Ref: shishi_as_rep296066
Ref: shishi_as_rep_process296423
Ref: shishi_as_rep_build296982
Ref: shishi_as_rep_der297316
Ref: shishi_as_rep_set297833
Ref: shishi_as_rep_der_set298102
Ref: shishi_as_krberror298571
Ref: shishi_as_krberror_der298941
Ref: shishi_as_krberror_set299482
Ref: shishi_as_tkt299762
Ref: shishi_as_tkt_set300090
Ref: shishi_as_sendrecv_hint300355
Ref: shishi_as_sendrecv300936
Node: TGS Functions302108
Ref: shishi_tgs304361
Ref: shishi_tgs_done304700
Ref: shishi_tgs_tgtkt305033
Ref: shishi_tgs_tgtkt_set305401
Ref: shishi_tgs_ap305691
Ref: shishi_tgs_req306031
Ref: shishi_tgs_req_set306373
Ref: shishi_tgs_req_der306657
Ref: shishi_tgs_req_der_set307192
Ref: shishi_tgs_req_process307678
Ref: shishi_tgs_req_build308052
Ref: shishi_tgs_rep308362
Ref: shishi_tgs_rep_der308699
Ref: shishi_tgs_rep_process309234
Ref: shishi_tgs_rep_build309608
Ref: shishi_tgs_krberror310010
Ref: shishi_tgs_krberror_der310364
Ref: shishi_tgs_krberror_set310912
Ref: shishi_tgs_tkt311201
Ref: shishi_tgs_tkt_set311543
Ref: shishi_tgs_sendrecv_hint311833
Ref: shishi_tgs_sendrecv312416
Ref: shishi_tgs_set_server312794
Ref: shishi_tgs_set_realm313155
Ref: shishi_tgs_set_realmserver313524
Node: Ticket (ASN.1) Functions313932
Ref: shishi_ticket315183
Ref: shishi_ticket_realm_get315516
Ref: shishi_ticket_realm_set316008
Ref: shishi_ticket_server316424
Ref: shishi_ticket_sname_set317341
Ref: shishi_ticket_get_enc_part_etype317924
Ref: shishi_ticket_set_enc_part318362
Ref: shishi_ticket_add_enc_part319271
Ref: shishi_encticketpart_get_key319823
Ref: shishi_encticketpart_key_set320263
Ref: shishi_encticketpart_flags_set320777
Ref: shishi_encticketpart_crealm_set321237
Ref: shishi_encticketpart_cname_set321693
Ref: shishi_encticketpart_transited_set322305
Ref: shishi_encticketpart_authtime_set322975
Ref: shishi_encticketpart_endtime_set323482
Ref: shishi_encticketpart_client323975
Ref: shishi_encticketpart_clientrealm324939
Node: AS/TGS Functions325876
Ref: shishi_as_derive_salt328943
Ref: shishi_kdcreq_sendrecv_hint329907
Ref: shishi_kdcreq_sendrecv330778
Ref: shishi_kdc_copy_crealm331521
Ref: shishi_as_check_crealm332080
Ref: shishi_kdc_copy_cname332802
Ref: shishi_as_check_cname333355
Ref: shishi_kdc_copy_nonce334057
Ref: shishi_kdc_check_nonce334576
Ref: shishi_tgs_process335386
Ref: shishi_as_process336506
Ref: shishi_kdc_process337403
Ref: shishi_asreq338799
Ref: shishi_tgsreq339105
Ref: shishi_kdcreq_print339426
Ref: shishi_kdcreq_save339823
Ref: shishi_kdcreq_to_file340210
Ref: shishi_kdcreq_parse340783
Ref: shishi_kdcreq_read341240
Ref: shishi_kdcreq_from_file341692
Ref: shishi_kdcreq_nonce_set342259
Ref: shishi_kdcreq_set_cname342697
Ref: shishi_kdcreq_client343270
Ref: shishi_asreq_clientrealm344185
Ref: shishi_kdcreq_realm345120
Ref: shishi_kdcreq_set_realm345992
Ref: shishi_kdcreq_server346416
Ref: shishi_kdcreq_set_sname347329
Ref: shishi_kdcreq_till347892
Ref: shishi_kdcreq_tillc348848
Ref: shishi_kdcreq_etype349262
Ref: shishi_kdcreq_set_etype349777
Ref: shishi_kdcreq_options350369
Ref: shishi_kdcreq_forwardable_p350815
Ref: shishi_kdcreq_forwarded_p351498
Ref: shishi_kdcreq_proxiable_p352308
Ref: shishi_kdcreq_proxy_p352966
Ref: shishi_kdcreq_allow_postdate_p353695
Ref: shishi_kdcreq_postdated_p354406
Ref: shishi_kdcreq_renewable_p355213
Ref: shishi_kdcreq_disable_transited_check_p356032
Ref: shishi_kdcreq_renewable_ok_p357162
Ref: shishi_kdcreq_enc_tkt_in_skey_p358052
Ref: shishi_kdcreq_renew_p358718
Ref: shishi_kdcreq_validate_p359605
Ref: shishi_kdcreq_options_set360556
Ref: shishi_kdcreq_options_add361042
Ref: shishi_kdcreq_clear_padata361519
Ref: shishi_kdcreq_get_padata361884
Ref: shishi_kdcreq_get_padata_tgs362699
Ref: shishi_kdcreq_add_padata363346
Ref: shishi_kdcreq_add_padata_tgs364202
Ref: shishi_kdcreq_add_padata_preauth364834
Ref: shishi_asrep365257
Ref: shishi_tgsrep365563
Ref: shishi_kdcrep_print365884
Ref: shishi_kdcrep_save366281
Ref: shishi_kdcrep_to_file366668
Ref: shishi_kdcrep_parse367241
Ref: shishi_kdcrep_read367698
Ref: shishi_kdcrep_from_file368150
Ref: shishi_kdcrep_crealm_set368719
Ref: shishi_kdcrep_cname_set369158
Ref: shishi_kdcrep_client_set369732
Ref: shishi_kdcrep_get_enc_part_etype370228
Ref: shishi_kdcrep_get_ticket370664
Ref: shishi_kdcrep_set_ticket371100
Ref: shishi_kdcrep_set_enc_part371536
Ref: shishi_kdcrep_add_enc_part372446
Ref: shishi_kdcrep_clear_padata373186
Ref: shishi_enckdcreppart_get_key373559
Ref: shishi_enckdcreppart_key_set374092
Ref: shishi_enckdcreppart_nonce_set374606
Ref: shishi_enckdcreppart_flags_set375057
Ref: shishi_enckdcreppart_authtime_set375509
Ref: shishi_enckdcreppart_starttime_set376020
Ref: shishi_enckdcreppart_endtime_set376588
Ref: shishi_enckdcreppart_renew_till_set377097
Ref: shishi_enckdcreppart_srealm_set377668
Ref: shishi_enckdcreppart_sname_set378155
Ref: shishi_enckdcreppart_populate_encticketpart378795
Node: Authenticator Functions379343
Ref: shishi_authenticator380254
Ref: shishi_authenticator_subkey380699
Ref: shishi_authenticator_print381180
Ref: shishi_authenticator_save381666
Ref: shishi_authenticator_to_file382137
Ref: shishi_authenticator_parse382767
Ref: shishi_authenticator_read383285
Ref: shishi_authenticator_from_file383798
Ref: shishi_authenticator_set_crealm384424
Ref: shishi_authenticator_set_cname384922
Ref: shishi_authenticator_client_set385567
Ref: shishi_authenticator_ctime386076
Ref: shishi_authenticator_ctime_set386588
Ref: shishi_authenticator_cusec_get387096
Ref: shishi_authenticator_cusec_set387618
Ref: shishi_authenticator_seqnumber_get388137
Ref: shishi_authenticator_seqnumber_remove388674
Ref: shishi_authenticator_seqnumber_set389124
Ref: shishi_authenticator_client389659
Ref: shishi_authenticator_clientrealm390623
Ref: shishi_authenticator_cksum391615
Ref: shishi_authenticator_set_cksum392399
Ref: shishi_authenticator_add_cksum393393
Ref: shishi_authenticator_add_cksum_type394164
Ref: shishi_authenticator_clear_authorizationdata395029
Ref: shishi_authenticator_add_authorizationdata395514
Ref: shishi_authenticator_authorizationdata396188
Ref: shishi_authenticator_remove_subkey396977
Ref: shishi_authenticator_get_subkey397407
Ref: shishi_authenticator_set_subkey397974
Ref: shishi_authenticator_add_random_subkey398985
Ref: shishi_authenticator_add_random_subkey_etype399522
Ref: shishi_authenticator_add_subkey400083
Node: KRB-ERROR Functions400508
Ref: shishi_krberror401762
Ref: shishi_krberror_print402093
Ref: shishi_krberror_save402509
Ref: shishi_krberror_to_file402909
Ref: shishi_krberror_parse403496
Ref: shishi_krberror_read403967
Ref: shishi_krberror_from_file404433
Ref: shishi_krberror_build405006
Ref: shishi_krberror_der405474
Ref: shishi_krberror_crealm406047
Ref: shishi_krberror_remove_crealm406585
Ref: shishi_krberror_set_crealm406977
Ref: shishi_krberror_client407424
Ref: shishi_krberror_set_cname408121
Ref: shishi_krberror_remove_cname408720
Ref: shishi_krberror_client_set409111
Ref: shishi_krberror_realm409585
Ref: shishi_krberror_set_realm410116
Ref: shishi_krberror_server410583
Ref: shishi_krberror_remove_sname411283
Ref: shishi_krberror_set_sname411831
Ref: shishi_krberror_server_set412426
Ref: shishi_krberror_ctime412900
Ref: shishi_krberror_ctime_set413352
Ref: shishi_krberror_remove_ctime413816
Ref: shishi_krberror_cusec414195
Ref: shishi_krberror_cusec_set414663
Ref: shishi_krberror_remove_cusec415130
Ref: shishi_krberror_stime415509
Ref: shishi_krberror_stime_set415961
Ref: shishi_krberror_susec416411
Ref: shishi_krberror_susec_set416879
Ref: shishi_krberror_errorcode417340
Ref: shishi_krberror_errorcode_fast417796
Ref: shishi_krberror_errorcode_set418257
Ref: shishi_krberror_etext418709
Ref: shishi_krberror_set_etext419243
Ref: shishi_krberror_remove_etext419731
Ref: shishi_krberror_edata420119
Ref: shishi_krberror_methoddata420655
Ref: shishi_krberror_set_edata421243
Ref: shishi_krberror_remove_edata421731
Ref: shishi_krberror_pretty_print422133
Ref: shishi_krberror_errorcode_message422624
Ref: shishi_krberror_message423108
Node: Cryptographic Functions423633
Ref: shishi_key_principal424598
Ref: shishi_key_principal_set424996
Ref: shishi_key_realm425401
Ref: shishi_key_realm_set425767
Ref: shishi_key_type426168
Ref: shishi_key_type_set426446
Ref: shishi_key_value426691
Ref: shishi_key_value_set427018
Ref: shishi_key_version427471
Ref: shishi_key_version_set427808
Ref: shishi_key_timestamp428133
Ref: shishi_key_timestamp_set428558
Ref: shishi_key_name428913
Ref: shishi_key_length429162
Ref: shishi_key429418
Ref: shishi_key_done429778
Ref: shishi_key_copy429985
Ref: shishi_key_from_value430323
Ref: shishi_key_from_base64430950
Ref: shishi_key_random431669
Ref: shishi_key_from_random432225
Ref: shishi_key_from_string432918
Ref: shishi_key_from_name433912
Ref: shishi_keys435211
Ref: shishi_keys_done435539
Ref: shishi_keys_size435866
Ref: shishi_keys_nth436121
Ref: shishi_keys_remove436614
Ref: shishi_keys_add436937
Ref: shishi_keys_print437381
Ref: shishi_keys_to_file437694
Ref: shishi_keys_from_file438257
Ref: shishi_keys_for_serverrealm_in_file438771
Ref: shishi_keys_for_server_in_file439458
Ref: shishi_keys_for_localservicerealm_in_file440038
Ref: shishi_hostkeys_default_file441153
Ref: shishi_hostkeys_default_file_set441569
Ref: shishi_hostkeys_for_server442093
Ref: shishi_hostkeys_for_serverrealm442614
Ref: shishi_hostkeys_for_localservicerealm443240
Ref: shishi_hostkeys_for_localservice443970
Ref: shishi_cipher_supported_p444750
Ref: shishi_cipher_name445007
Ref: shishi_cipher_blocksize445339
Ref: shishi_cipher_confoundersize445645
Ref: shishi_cipher_keylen446046
Ref: shishi_cipher_randomlen446354
Ref: shishi_cipher_defaultcksumtype446758
Ref: shishi_cipher_parse447098
Ref: shishi_checksum_supported_p447410
Ref: shishi_checksum_name447684
Ref: shishi_checksum_cksumlen448004
Ref: shishi_checksum_parse448321
Ref: shishi_string_to_key448656
Ref: shishi_random_to_key449769
Ref: shishi_checksum450432
Ref: shishi_verify451370
Ref: shishi_encrypt_ivupdate_etype452289
Ref: shishi_encrypt_iv_etype454233
Ref: shishi_encrypt_etype455970
Ref: shishi_encrypt_ivupdate457649
Ref: shishi_encrypt_iv459474
Ref: shishi_encrypt461086
Ref: shishi_decrypt_ivupdate_etype462631
Ref: shishi_decrypt_iv_etype464575
Ref: shishi_decrypt_etype466312
Ref: shishi_decrypt_ivupdate467960
Ref: shishi_decrypt_iv469785
Ref: shishi_decrypt471403
Ref: shishi_n_fold472916
Ref: shishi_dr473793
Ref: shishi_dk474589
Ref: shishi_crypto475570
Ref: shishi_crypto_encrypt476733
Ref: shishi_crypto_decrypt477586
Ref: shishi_crypto_close478435
Ref: shishi_randomize478894
Ref: shishi_crc479417
Ref: shishi_md4480153
Ref: shishi_md5480676
Ref: shishi_hmac_md5481209
Ref: shishi_hmac_sha1481936
Ref: shishi_des_cbc_mac482669
Ref: shishi_arcfour483411
Ref: shishi_des484681
Ref: shishi_3des485644
Ref: shishi_aes_cts486615
Ref: shishi_pbkdf2_sha1487760
Node: X.509 Functions488870
Ref: shishi_x509ca_default_file_guess489219
Ref: shishi_x509ca_default_file_set489671
Ref: shishi_x509ca_default_file490311
Ref: shishi_x509cert_default_file_guess490839
Ref: shishi_x509cert_default_file_set491314
Ref: shishi_x509cert_default_file491972
Ref: shishi_x509key_default_file_guess492515
Ref: shishi_x509key_default_file_set492959
Ref: shishi_x509key_default_file493588
Node: Utility Functions494027
Ref: shishi_realm_default_guess494249
Ref: shishi_realm_default495371
Ref: shishi_realm_default_set495875
Ref: shishi_realm_for_server_file496484
Ref: shishi_realm_for_server_dns496924
Ref: shishi_realm_for_server498481
Ref: shishi_principal_default_guess499224
Ref: shishi_principal_default499650
Ref: shishi_principal_default_set500191
Ref: shishi_parse_name500676
Ref: shishi_principal_name501492
Ref: shishi_principal_name_realm502499
Ref: shishi_principal_name_set503726
Ref: shishi_principal_set504420
Ref: shishi_derive_default_salt505026
Ref: shishi_server_for_local_service505560
Ref: shishi_authorize_strcmp506187
Ref: shishi_authorize_k5login506782
Ref: shishi_authorization_parse507364
Ref: shishi_authorized_p507693
Ref: shishi_generalize_time508463
Ref: shishi_generalize_now508939
Ref: shishi_generalize_ctime509355
Ref: shishi_time509760
Ref: shishi_ctime510395
Ref: shishi_prompt_password_callback_set511197
Ref: shishi_prompt_password_callback_get512086
Ref: shishi_prompt_password512524
Ref: shishi_resolv513198
Ref: shishi_resolv_free513853
Node: ASN.1 Functions514079
Ref: shishi_asn1_read_inline514290
Ref: shishi_asn1_read515243
Ref: shishi_asn1_read_optional516100
Ref: shishi_asn1_done517061
Ref: shishi_asn1_pa_enc_ts_enc517418
Ref: shishi_asn1_encrypteddata517716
Ref: shishi_asn1_padata517999
Ref: shishi_asn1_methoddata518278
Ref: shishi_asn1_etype_info518565
Ref: shishi_asn1_etype_info2518853
Ref: shishi_asn1_asreq519131
Ref: shishi_asn1_asrep519398
Ref: shishi_asn1_tgsreq519667
Ref: shishi_asn1_tgsrep519938
Ref: shishi_asn1_apreq520207
Ref: shishi_asn1_aprep520474
Ref: shishi_asn1_encapreppart520755
Ref: shishi_asn1_ticket521031
Ref: shishi_asn1_encticketpart521315
Ref: shishi_asn1_authenticator521613
Ref: shishi_asn1_enckdcreppart521911
Ref: shishi_asn1_encasreppart522207
Ref: shishi_asn1_krberror522493
Ref: shishi_asn1_krbsafe522770
Ref: shishi_asn1_priv523039
Ref: shishi_asn1_encprivpart523319
Ref: shishi_asn1_to_der_field523614
Ref: shishi_asn1_to_der524355
Ref: shishi_asn1_msgtype524997
Ref: shishi_der_msgtype525490
Ref: shishi_der2asn1525945
Ref: shishi_der2asn1_padata526458
Ref: shishi_der2asn1_methoddata526959
Ref: shishi_der2asn1_etype_info527468
Ref: shishi_der2asn1_etype_info2527978
Ref: shishi_der2asn1_ticket528480
Ref: shishi_der2asn1_encticketpart528986
Ref: shishi_der2asn1_asreq529490
Ref: shishi_der2asn1_tgsreq529981
Ref: shishi_der2asn1_asrep530472
Ref: shishi_der2asn1_tgsrep530963
Ref: shishi_der2asn1_kdcrep531456
Ref: shishi_der2asn1_encasreppart531961
Ref: shishi_der2asn1_enctgsreppart532479
Ref: shishi_der2asn1_enckdcreppart532999
Ref: shishi_der2asn1_authenticator533519
Ref: shishi_der2asn1_krberror534029
Ref: shishi_der2asn1_krbsafe534528
Ref: shishi_der2asn1_priv535019
Ref: shishi_der2asn1_encprivpart535521
Ref: shishi_der2asn1_apreq536024
Ref: shishi_der2asn1_aprep536513
Ref: shishi_der2asn1_encapreppart537016
Ref: shishi_der2asn1_kdcreq537520
Ref: shishi_asn1_print538022
Node: Error Handling538385
Node: Error Values539146
Node: Error Functions544165
Ref: shishi_strerror544332
Ref: shishi_error544733
Ref: shishi_error_clear545227
Ref: shishi_error_set545781
Ref: shishi_error_printf546467
Ref: shishi_error_outputtype547017
Ref: shishi_error_set_outputtype547495
Ref: shishi_info548114
Ref: shishi_warn548484
Ref: shishi_verbose548845
Node: Examples549324
Node: Kerberos Database Functions550958
Ref: shisa553649
Ref: shisa_done553910
Ref: shisa_init554192
Ref: shisa_init_with_paths554957
Ref: shisa_cfg_db556055
Ref: shisa_cfg556770
Ref: shisa_cfg_from_file557471
Ref: shisa_cfg_default_systemfile557933
Ref: shisa_enumerate_realms558666
Ref: shisa_enumerate_principals559394
Ref: shisa_principal_find560308
Ref: shisa_principal_update561108
Ref: shisa_principal_add562422
Ref: shisa_principal_remove563371
Ref: shisa_keys_find564061
Ref: shisa_key_add565269
Ref: shisa_key_update565884
Ref: shisa_key_remove567395
Ref: shisa_key_free568428
Ref: shisa_keys_free568746
Ref: shisa_strerror569416
Ref: shisa_info569819
Node: Generic Security Service570152
Node: Acknowledgements571397
Node: Criticism of Kerberos572120
Node: Protocol Extensions574367
Node: STARTTLS protected KDC exchanges575187
Node: Telnet encryption with AES-CCM584317
Node: Kerberized rsh and rlogin592272
Ref: basic authorization596677
Ref: kerberos authorization604162
Node: Key as initialization vector606356
Ref: Key as initialization vector-Footnote-1608715
Node: The Keytab Binary File Format608881
Node: The Credential Cache Binary File Format613721
Node: Copying Information617141
Node: GNU Free Documentation License617403
Node: Function and Data Index642518
Node: Concept Index719565

End Tag Table


Local Variables:
coding: utf-8
End:
