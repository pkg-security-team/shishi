@subheading shisa
@anchor{shisa}
@deftypefun {Shisa *} {shisa} ()

@strong{Description:} Initializes the Shisa library.  If this function fails, it may
print diagnostic errors to standard error.

@strong{Return value:} Returns a Shisa library handle, or @code{NULL} on error.
@end deftypefun

@subheading shisa_done
@anchor{shisa_done}
@deftypefun {void} {shisa_done} (@w{Shisa * @var{dbh}})
@var{dbh}: Shisa handle as allocated by @code{shisa()}.

@strong{Description:} Deallocates the shisa library handle.  The handle must not be used
in calls to any shisa function after the completion of this call.
@end deftypefun

@subheading shisa_init
@anchor{shisa_init}
@deftypefun {int} {shisa_init} (@w{Shisa ** @var{dbh}})
@var{dbh}: Returned pointer to a created Shisa library handle.

@strong{Description:} Creates a Shisa library handle, using @code{shisa()}, reading the system
configuration file from its default location.  The path to the
default system configuration file is decided at compile time
(@env{sysconfdir}/shisa.conf).

The handle is allocated regardless of return value, the only
exception being @code{SHISA_INIT_ERROR}, which indicates a problem
in allocating the handle.  Other error conditions arise while
reading a file.

@strong{Return value:} Returns @code{SHISA_OK}, or an error code.  The value
@code{SHISA_INIT_ERROR} indicates a failure to create the handle.
@end deftypefun

@subheading shisa_init_with_paths
@anchor{shisa_init_with_paths}
@deftypefun {int} {shisa_init_with_paths} (@w{Shisa ** @var{dbh}}, @w{const char * @var{file}})
@var{dbh}: Returned pointer to a created Shisa library handle.
@*@var{file}: Filename of system configuration, or @code{NULL}.

@strong{Description:} Creates a Shisa library handle, using @code{shisa()}, but reading
the system configuration file at the location @var{file}, or at
the default location, should @var{file} be @code{NULL}.  The path to
the default system configuration file is decided at compile
time (@env{sysconfdir}/shisa.conf).

The handle is allocated regardless of return value, the only
exception being @code{SHISA_INIT_ERROR}, which indicates a problem
in allocating the handle.  Other error conditions arise while
reading a file.

@strong{Return value:} Returns @code{SHISA_OK}, or an error code.  The value
@code{SHISA_INIT_ERROR} indicates a failure to create the handle.
@end deftypefun

