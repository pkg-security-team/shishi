@subheading shishi_realm_default_guess
@anchor{shishi_realm_default_guess}
@deftypefun {char *} {shishi_realm_default_guess} ()

@strong{Description:} Guesses a realm based on @code{getdomainname()}, which really responds
with a NIS/YP domain, but if set properly, it might be a good
first guess.  If this NIS query fails, call @code{gethostname()},
and on its failure, fall back to returning the artificial
string "could-not-guess-default-realm".

Note that the hostname is not trimmed off of the string returned
by @code{gethostname()}, thus pretending the local host name is a valid
realm name.  The resulting corner case could merit a check that
the suggested realm is distinct from the fully qualifies host,
and if not, simply strip the host name from the returned string
before it is used in an application.  One reason for sticking
with the present behaviour, is that some systems respond with
a non-qualified host name as reply from @code{gethostname()}.

@strong{Return value:} Returns a guessed realm for the running host,
containing a string that has to be deallocated with
@code{free()} by the caller.
@end deftypefun

@subheading shishi_realm_default
@anchor{shishi_realm_default}
@deftypefun {const char *} {shishi_realm_default} (@w{Shishi * @var{handle}})
@var{handle}: Shishi library handle created by @code{shishi_init()}.

@strong{Description:} Determines name of default realm, i.e., the name of whatever
realm the library will use whenever an explicit realm is not
stated during a library call.

@strong{Return value:} Returns the default realm in use by the library.
Not a copy, so do not modify or deallocate the returned string.
@end deftypefun

@subheading shishi_realm_default_set
@anchor{shishi_realm_default_set}
@deftypefun {void} {shishi_realm_default_set} (@w{Shishi * @var{handle}}, @w{const char * @var{realm}})
@var{handle}: Shishi library handle created by @code{shishi_init()}.
@*@var{realm}: String stating a new default realm name, or @code{NULL}.

@strong{Description:} Sets the default realm used by the library; or, with @var{realm}
set to @code{NULL}, resets the library realm setting to that name
selected by configuration for default value.

The string is copied into the library, so you can dispose of
the content in @var{realm} immediately after calling this function.
@end deftypefun

@subheading shishi_realm_for_server_file
@anchor{shishi_realm_for_server_file}
@deftypefun {char *} {shishi_realm_for_server_file} (@w{Shishi * @var{handle}}, @w{char * @var{server}})
@var{handle}: Shishi library handle created by @code{shishi_init()}.
@*@var{server}: Hostname to determine realm for.

@strong{Description:} Finds the realm applicable to a host @var{server}, using the
standard configuration file.

@strong{Return value:} Returns realm for host, or @code{NULL} if not known.
@end deftypefun

@subheading shishi_realm_for_server_dns
@anchor{shishi_realm_for_server_dns}
@deftypefun {char *} {shishi_realm_for_server_dns} (@w{Shishi * @var{handle}}, @w{char * @var{server}})
@var{handle}: Shishi library handle created by @code{shishi_init()}.
@*@var{server}: Hostname to find realm for.

@strong{Description:} Finds the realm for a host @var{server} using DNS lookup, as is
prescribed in "draft-ietf-krb-wg-krb-dns-locate-03.txt".

Since DNS lookup can be spoofed, relying on the realm information
may result in a redirection attack.  In a single-realm scenario,
this only achieves a denial of service, but with trust across
multiple realms the attack may redirect you to a compromised realm.
For this reason, Shishi prints a warning, suggesting that the user
should instead add a proper 'server-realm' configuration token.

To illustrate the DNS information used, here is an extract from a
zone file for the domain ASDF.COM:

_kerberos.asdf.com.             IN   TXT     "ASDF.COM"
_kerberos.mrkserver.asdf.com.   IN   TXT     "MARKETING.ASDF.COM"
_kerberos.salesserver.asdf.com. IN   TXT     "SALES.ASDF.COM"

Let us suppose that in this case, a client wishes to use a service
on the host "foo.asdf.com".  It would first query for

_kerberos.foo.asdf.com.  IN TXT

Finding no match, it would then query for

_kerberos.asdf.com.      IN TXT

With the resource records stated above, the latter query returns
a positive answer.

@strong{Return value:} Returns realm for the indicated host, or @code{NULL}
if no relevant TXT record could be found.
@end deftypefun

@subheading shishi_realm_for_server
@anchor{shishi_realm_for_server}
@deftypefun {char *} {shishi_realm_for_server} (@w{Shishi * @var{handle}}, @w{char * @var{server}})
@var{handle}: Shishi library handle created by @code{shishi_init()}.
@*@var{server}: Hostname to find realm for.

@strong{Description:} Finds a realm for the host @var{server}, using various methods.

Currently this includes static configuration files, using
the library call @code{shishi_realm_for_server_file()}, and DNS
lookup using @code{shishi_realm_for_server_dns()}.  They are
attempted in the stated order.  See the documentation of
either function for more information.

@strong{Return value:} Returns realm for the indicated host, or @code{NULL}
if nothing is known about @var{server}.
@end deftypefun

