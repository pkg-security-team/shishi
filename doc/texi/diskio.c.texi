@subheading shishi_key_print
@anchor{shishi_key_print}
@deftypefun {int} {shishi_key_print} (@w{Shishi * @var{handle}}, @w{FILE * @var{fh}}, @w{const Shishi_key * @var{key}})
@var{handle}: Shishi handle as allocated by @code{shishi_init()}.
@*@var{fh}: File handle open for writing.
@*@var{key}: Key to print.

@strong{Description:} Prints an ASCII representation of a key structure @var{key}
to the file descriptor @var{fh}.  Example output:

-----BEGIN SHISHI KEY-----

@strong{Keytype:} 18 (aes256-cts-hmac-sha1-96)

@strong{Principal:} host/latte.josefsson.org

@strong{Realm:} JOSEFSSON.ORG
Key-Version-Number: 1

@strong{Timestamp:} 20130420150337Z

P1QdeW/oSiag/bTyVEBAY2msiGSTmgLXlopuCKoppDs=
-----END SHISHI KEY-----

@strong{Return value:} Returns @code{SHISHI_OK} if successful.
The only failure is @code{SHISHI_MALLOC_ERROR}.
@end deftypefun

@subheading shishi_key_to_file
@anchor{shishi_key_to_file}
@deftypefun {int} {shishi_key_to_file} (@w{Shishi * @var{handle}}, @w{const char * @var{filename}}, @w{Shishi_key * @var{key}})
@var{handle}: Shishi handle as allocated by @code{shishi_init()}.
@*@var{filename}: Name of file, to which the key text is appended.
@*@var{key}: Key to print.

@strong{Description:} Prints an ASCII representation of a key structure @var{key}
to the file @var{filename}.  The text is appended if the file
exists.  See @code{shishi_key_print()} for an example of output text.

@strong{Return value:} Returns @code{SHISHI_OK} if successful.
Failures are due to I/O issues, or to allocation.
@end deftypefun

